import HeaderVitta from "../../components/Atoms/Header";
import Footer from "../../components/Atoms/Footer";

import { Container } from "./styled";
import Images from "../../components/Molecules/ImageHome";
const Home = () => {
  return (
    <>
      <HeaderVitta />
      <Container>
        <Images />
        <Footer />
      </Container>
    </>
  );
};

export default Home;
