import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  color: whitesmoke;
  justify-content: space-evenly;
  padding-bottom: -15%;
  font-family: "Campton", sans-serif;
  font-size: 11px;
`;
