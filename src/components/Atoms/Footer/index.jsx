import { Container } from "./styled";
import Logo from "../Header/Subtract.svg";
const Footer = () => {
  return (
    <>
      <Container>
        <img src={Logo}></img>
        <div>FAQ</div>
        <div>NOSSA HISTÓRIA</div>
        <div>NOSSAS VAGAS</div>
        <div>VITTA NETWORK</div>
        <div>NOSSO BLOG</div>
        <div style={{ width: "100px" }}></div>
      </Container>
    </>
  );
};

export default Footer;
