import styled from "styled-components";

export const DivImage = styled.div`
  width: 32.9vw;
  height: 45vw;
  background-color: rgba(10, 10, 10, 1);
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  div:nth-child(1) {
    font-family: "Campton", Sans-serif;
    font-size: 37px;
    font-weight: bold;
    padding: 25%;
    padding-bottom: 0;
    padding-top: 50%;

    font-weight: 700;
    line-height: 1em;
    letter-spacing: -0.8px;
  }
  div:nth-child(2) {
    font-family: "Campton", sans-serif;
    font-size: 17px;
    padding-left: 25%;
    padding-right: 25%;
  }
  &:hover {
    background-color: rgba(0, 0, 0, 0);
  }
  transition: background 0.5s;
`;
export const ButtonAcess = styled.button`
  width: 10vw;
  height: 5vh;
  background-color: #017bff;
  border-radius: 35px;
  border: 0;
  font-family: "Campton", Sans-serif;
  color: white;
  margin-left: 25%;
  margin-bottom: 45px;
`;

export const Container = styled.div`
  height: 62vh;
`;
