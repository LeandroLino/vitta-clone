import { DivImage, ButtonAcess, Container } from "./styled";
const Images = ({ props }) => {
  return (
    <Container>
      {props === "Boy" ? (
        <DivImage>
          <div style={{ color: "white", alignItems: "flex-start" }}>
            PLANOS DE SAÚDE PARA STARTUPS
          </div>
          <div style={{ color: "white", alignItems: "flex-start" }}>
            Os primeiros do Brasil com remédios de farmácia.
          </div>
          <ButtonAcess>ACESSAR</ButtonAcess>
        </DivImage>
      ) : props === "Doctor" ? (
        <DivImage>
          <div style={{ color: "white", alignItems: "flex-start" }}>
            CONSULTORIA DE BENEFÍCIOS
          </div>
          <div style={{ color: "white", alignItems: "flex-start" }}>
            Um modelo high tech e high touch de gestão de saúde.
          </div>
          <ButtonAcess>ACESSAR</ButtonAcess>
        </DivImage>
      ) : (
        <DivImage>
          <div style={{ color: "white", alignItems: "flex-start" }}>
            TELEMEDICINA CLINICWEB
          </div>
          <div style={{ color: "white", alignItems: "flex-start" }}>
            O melhor prontuário eletrônico do mercado, agora com telemedicina.
          </div>
          <ButtonAcess>ACESSAR</ButtonAcess>
        </DivImage>
      )}
    </Container>
  );
};
export default Images;
