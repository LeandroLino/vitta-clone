import styled from "styled-components";

export const Container = styled.div`
  height: 15vh;
  width: auto;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #111111;
  color: whitesmoke;
  font-size: 20px;
  > div {
    margin-left: 15px;
  }
  > img {
    height: 7vh;
    width: 7vw;
  }
  > img,
  div {
    padding-top: 15px;
  }
`;
