import Logo from "./Subtract.svg";
import { Container } from "./styled";
const HeaderVitta = () => {
  return (
    <Container>
      <img src={Logo}></img>
      <div>VITTA</div>
    </Container>
  );
};
export default HeaderVitta;
