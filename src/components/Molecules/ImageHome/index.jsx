import { Container } from "./styled";
import Images from "../../Atoms/Images";
const Imagens = () => {
  return (
    <Container>
      <Images props="Boy" />
      <Images props="Doctor" />
      <Images props="Patient" />
    </Container>
  );
};
export default Imagens;
