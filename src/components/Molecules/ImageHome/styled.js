import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  width: auto;
  height: 88vh;
  > {
    height: auto;
  }
`;
